<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/../public/DBQuery.php");
$user_auth = new DBQuery();

if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $user = $user_auth->register($email, $password);
} else {
  header("location:index.php");
}
?>
