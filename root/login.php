<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/../public/DBQuery.php");
$user_auth = new DBQuery();

session_start();

if (array_key_exists('email', $_POST) && array_key_exists('password', $_POST)) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $user = $user_auth->login($email, $password);
} else {
  header("location:index.php");
  /*if (isset($_SESSION['id']) !== null) {
    echo "not null" ;
  } else {
    echo 'null';
  }*/
}
?>
