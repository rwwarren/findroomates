<?php
$root = realpath($_SERVER['DOCUMENT_ROOT']);
require_once("$root/../public/PublicIndex.php");
require_once("$root/../public/PrivateIndex.php");

 session_start();

  if ($_SESSION['id'] !== null) {
    $page = new PrivateHome;
    $page->render();
  } else {
    $page = new PublicHome;
    $page->render();
  }
?>
