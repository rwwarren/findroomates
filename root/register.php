<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/../public/Page.php");
class Reg extends Page {
  
  public function pageTitle() {
    return 'Registration';
  }

  public function headerContent() {
    return '';
  }

  public function bodyContent() {
    return 
      '<form method="post" action="reg_check.php">' .
      'Email: <input type="text" name="email" />' .
      'Password: <input type="password" name="password" />' .
      '<input type="submit" value="Register" />' .
      '</form>' ;
  }

  public function footerContent() {
    return '';
  }
}
$page = new Reg;
$page->render();
?>
