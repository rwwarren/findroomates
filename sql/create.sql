
CREATE DATABASE Findroomates;
USE Findroomates
CREATE USER 'ryan'@'localhost' IDENTIFIED BY 'ba3ras6c';
GRANT ALL ON Findroomates.* TO 'ryan'@'localhost';
CREATE USER 'fr_r'@'localhost' IDENTIFIED BY '[j9Xda9S';
GRANT SELECT ON Findroomates.* TO 'fr_r'@'localhost';
CREATE USER 'fr_w'@'localhost' IDENTIFIED BY 'etE8PTTP';
GRANT SELECT, INSERT ON Findroomates.* TO 'fr_w'@'localhost';
CREATE USER 'fr_d'@'localhost' IDENTIFIED BY 'p8lmiTtK';
GRANT SELECT, DELETE ON Findroomates.* TO 'fr_d'@'localhost';
CREATE USER 'fr_up'@'localhost' IDENTIFIED BY 'p2<*Y1Bo';
GRANT SELECT, UPDATE ON Findroomates.* TO 'fr_up'@'localhost'

CREATE TABLE Users( 
  id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, 
  email VARCHAR(64), 
  password VARCHAR(64), 
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  PRIMARY KEY(id)
);
