<?php
require_once('PublicPage.php');

class PublicHome extends PublicPage {

  public function scripts() {
    return '';
  }

  public function pageTitle() {
    return 'Find Roomates - Index';
  }

  public function extraHeader() {
    return '';
  }

  public function headerContent() {
    return
      '<div id="login">' .
      '<form method="post" action="login.php">' .
      'Email: <input type="text" name="email" />' .
      ' Password: <input type="password" name="password" />' .
      '<input type="submit" value="Sign in" />' .
      '</form>' .
      '<div id="headernav">' .
      '</div>' .
      '</div>';
  }

  public function bodyContent() {
    return
      '<div id="leftnav">' .
      '<a href="register.php">Register Here</a>' .
      '</div>' .
      '<div id="rightnav">' .

      '</div>';
  }

  public function footerContent() {
    return
      '<div id="footernav">' .

      '</div>';
  }

}
?>
