<?php
require_once('Page.php');

abstract class PublicPage extends Page {

  public function generalScripts() {
    return '';
  }

  public function headerContent() {
    return
      '<div id="login">' .
      '<form method="post" action="login.php">' .
      'Email: <input type="text" name="email" />' .
      ' Password: <input type="password" name="password" />' .
      '<input type="submit" value="Sign in" />' .
      '</form>' .
      '<div id="headernav">' .
      '</div>' .
      '</div>';
  }

}

?>
