<?php

abstract class Page {

  public function pageTitle() {
    return 'Find Roomates';
  }

  public function isLoggedin() {
    if ($_SESSION['id'] !== null) {
      return true;
    } else {
      return false;
    }
  }

  final public function render() {
    date_default_timezone_set('America/Los_Angeles');
    //date_default_timezone_set(timezone);
    session_start();
    $content =
      '<!DOCTYPE html>'.
      '<link rel="stylesheet" type="text/css" href="/css/styles.css" />'.
      '<html>'.
      '<head>'.
      '<title>' . $this->pageTitle(). '</title>'.
      $this->generalScripts() .
      $this->scripts() .
      '</head>' .
      '<body>'.
      //fix the div centering
      '<div id="all">' .
      '<div id="header">' .
      '<div id="logo">' .
      'FindRoomates' .
      '</div>' .
      $this->headerContent() .
      $this->extraHeader() .
      '</div>' .
      '<div id="content">' .
      $this->bodyContent() .
      '</div>' .
      '<div id="footer">' .
      $this->footerContent() .
      '&copy ' .
      date("Y") .
      ' FindRoomates.com' .
      '</div>' .
      '</div>'.
      '</body>' .
      '</html>';
    echo $content;
  }

  //TODO create grandchildren
  //make public / private pages
  //abstract protected function pageTitle();
  abstract protected function headerContent();
  abstract protected function extraHeader();
  abstract protected function bodyContent();
  abstract protected function footerContent();
  abstract protected function generalScripts();
  abstract protected function scripts();

}

?>
